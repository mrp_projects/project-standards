0. Component-style Code
-----------------------

Optimize for modularity. Follow principles of OOCSS. I want my front-end code to behave like little lego blocks that I can snap together, rearrange, and extend. The key metric for success is this: If I copy and paste a snippet of code from one place to a new place, it should work, and behave the same way as it did before.

- We should almost never plan to style a thing based on where it is in the document. It is a very rare case that a thing is guaranteed to only ever behave in a certain way based on its ancestors.

- Prefer not to style HTML elements by tag name, unless the style is really guaranteed to be specific to that element. (A <strong> tag may carry meaning due to its tag name. A <ul> does not; it should be styled using a class name, not a tag name, in case it ever changes to a <ol>, for example.)

- "Components" should fill their container and clearfix themselves as necessary.


1. HTML
-------

- "DIV-itis" is okay. Components should scope themselves, protect their scope, fill their container, and be independent of the layout in which they are placed. If an unattractive mass of nested containers is necessary to make sure a component is robust and self-protecting, I prefer that instead of more "semantic" code that is less predictable/maintainable.

- Only use HTML5 elements if there is already a need to include Modernizer/shim scripts in the HEAD. Otherwise, we either lose support for older browsers, or we have to add a blocking script in the HEAD. <div class="section"> does just as well for us as <section>. We are not Zeldman, yet.

- Layout containers shouldn't receive aesthetic/theme styles. Prefer to plan for a separation between structural/layout containers and the components inside them.

- No IDs. Ever. Unless there really is guaranteed to only ever be exactly one of a thing. Which is almost never. Always assume there will be more than one of a thing. As soon as you decide that a thing is unique, the client will want to add another one.

- Class names should be in studlyCase.

- Use a single hyphen in class names to denote classes that belong to a group. This could be either a group of helpers (i.e. bg-blue, bg-red, bg-black) or a group of component classes that must be used together (i.e. iconButton, iconButton-icon, iconButton-label).

- Use a double hyphen in class names to denote a variation of a base component (i.e. iconButton, iconButton--light, iconButton--dark). Abstract and extend!


1. Styling
----------

- SCSS variables should only ever be created in a _variables.scss file, which is the very first file to be included in the compiled stylesheet.

- Properties that could be standardized between elements should be. Don't define these at the selector level; use a variable instead. (margin, padding, border-radius, font-size, font-weight, etc.

- Prefer to standardize dimensions that are only slightly different between elements in a mockup (i.e. one element that has 12px font, and one with 13px font), unless there is an obvious reason they should be different.

- Use the lobotomized owl.


2. Scripting
------------

- Never attach JavaScript to an ID or class. Always use data attributes and attribute selectors. (JS may manipulate classes for presentation, but not depend on them.)

- Prefer to define functions as named function expressions. (i.e. MySite.someMethod = function...)

- Name all functions; especially anonymous ones. (i.e. blah = function blah()...)

- Use a space after, but not before, the argument definition.

- Place the opening bracket of a function definition on the same line as the function name.

- Place all other block brackets on separate lines.

- If a method takes an object, array, or other method as an argument, place all arguments on separate lines, with array arguments as blocks (opening and closing brackets on separate lines, contents indented).