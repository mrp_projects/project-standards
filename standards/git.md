0. Commit messages
-----------------------

- Commit messages should be sentence-cased and punctuated.

- List each change the commit makes as a sentence fragment starting with a past-tense verb:

    * _Installed the new version of the brand icon font. Changed widget styling to fix a bug where the widgets appeared red instead of blue on the homepage._
    * _Added source maps to SCSS to enable easier debugging of compiled CSS styles._

- The diff tells me _what_. Your commit message must tell me _why_.

- Separate CMS updates and third-party component updates into separate commits. Their commit messages take special forms for consistent searching: Simply list the components installed/updated

    * _Craft Update: Craft Pro 2.4.2456_
    * _3P addition: Sprout Forms 1.0.1_
    * _3P update: Sprout Forms 1.0.2_
    * _Component addition: jQuery 1.2.1., Knockout 3.1.0, Slick 1.1.1_
    * _Component update: jQuery 1.2.2._
    * _Composer addition: MyLib 3.1_
    * _Composer update: MyLib 3.2_


1. Branches
-----------------------

The `master` branch runs the production server.

The `dev` branch represents the latest safe/deploy-ready code, and may also run the staging server on smaller projects.

On projects that require review by a non-developer (i.e. someone not tracking the dev branch) before something goes live, the `staging` branch runs the staging server.


2. Workflow
-----------------------

When possible, we commit `bower_components`, but don't use or link to them directly. They're simply there for archival. We make our own copy for use in the working directories.

When possible, we commit static assets, including user-generated content.
(This serves as a crude backup and makes syncing assets between servers part of our ordinary deployment process.)
(We're revisiting this.)

Always pull before you commit, and pull again before you push. Always.