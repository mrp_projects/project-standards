# MRP Project/Code Standards

These are the coding standards that Michael Rog & Partners uses (or at least aspires to use) to when building projects.

Their dual purpose is to codify best practices, and to create a set of conventions to make our project setup and code more familiar
to each other and to third parties.

If you find a bit that you think is worthy of additional discussion and possible revision, take it up with Michael. You're probably right.  :-)


The standards are comprised of the following documents:

* [PHP](standards/PHP.md)
* [Git](standards/git.md)
* [Misc](standards/misc.md)